<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', function (Request $request) {
    $json = $request->json->all();
    return response()->json();
});

Route::get('matchs/incoming', 'MatchController@NewMatch');
route::get('matchs/done', 'MatchController@MatchDone');
Route::get('teams', 'TeamController@getTeam');

Route::post('matchs/add', 'MatchController@create');
Route::put('matchs/{id}', 'MatchController@update');
Route::delete('matchs/{id}', 'MatchController@delete');

Route::post('teams/add', 'TeamController@create');
Route::put('teams/{id}', 'TeamController@update');
Route::delete('teams/{id}', 'TeamController@delete');
