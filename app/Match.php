<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Team;
class Match extends Model
{
    //
    public function team1()
    {
        return $this->belongsTo(Team::class,'team1' ,'id');
    }
    //
    public function team2()
    {
        return $this->belongsTo(Team::class,'team2' ,'id');
    }
}
