<?php

namespace App\Http\Controllers;

use App\Match;
use Illuminate\Http\Request;

class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function MatchDone()
    {
        $match = Match::where('id', '!=', '0')->with('team1', 'team2')->get();
        return $match;
    }
    public function NewMatch(Request $request)
    {

    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $match = new Match();
        $match->team1 = $request->team1;
        $match->team2 = $request->team2;
        $match->dateMatch = $request->dateMatch;
        $match->result = $request->result;
        $match->save();

        return response()->json([
            "match" => "match created",
        ], 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function show(Match $match)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Match::where('id', $id)->exists()) {
            $match = Match::find($id);
            $match->team1 = is_null($request->team1) ? $match->team1 : $request->team1;
            $match->team2 = is_null($request->team2) ? $match->team2 : $request->team2;
            $match->dateMatch = is_null($request->dateMatch) ? $match->dateMatch : $request->dateMatch;
            $match->result = is_null($request->result) ? $match->result : $request->result;
            $match->save();

            return response()->json([
                "match" => "match mis a jour",
            ], 200);
        } else {
            return response()->json([
                "match" => "match not found",
            ], 404);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if (Match::where('id', $id)->exists()) {
            $match = Match::find($id);
            $match->delete();

            return response()->json([
                "message" => "records deleted",
            ], 202);
        } else {
            return response()->json([
                "message" => "match not found",
            ], 404);
        }
    }
}
