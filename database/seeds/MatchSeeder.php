<?php

use Illuminate\Database\Seeder;

class MatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('matches')->insert(
            array(
                array(
                    'team1' => 1,
                    'team2' => 2,
                    'dateMatch' => '2020-03-06',
                    'result' => 'N'
                ),
                array(
                    'team1' => 2,
                    'team2' => 1,
                    'dateMatch' => '2020-03-12',
                    'result' => '1'
                ),
                array(
                    'team1' => 1,
                    'team2' => 2,
                    'dateMatch' => '2020-04-09',
                    'result' => '2'
                ),
                array(                    
                    'team1' => 2,
                    'team2' => 1,
                    'dateMatch' => '2020-02-12',
                    'result' => '1'
                )
            )
        );
    }
}
